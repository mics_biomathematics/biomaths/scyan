# scyan.data.load

```py
### Usage example
import scyan

adata, marker_pop_matrix = scyan.data.load("aml")
```

::: scyan.data.load
