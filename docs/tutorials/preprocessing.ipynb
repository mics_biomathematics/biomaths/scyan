{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Prepare your data\n",
    "\n",
    "This tutorial guides you from an FCS file to the creation of a `Scyan` object."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Creation of your `AnnData` object (cytometry data)\n",
    "\n",
    "Create an `AnnData` object containing your cytometry data. Consider reading the [anndata documentation](https://anndata.readthedocs.io/en/latest/) if you have never heard about `anndata` before.\n",
    "\n",
    "### Loading a `FCS` file\n",
    "\n",
    "You probably have `.fcs` files that you want to load. For this, you can use [`scyan.read_fcs`](../../api/read_fcs). Make sure you have already [installed scyan](../../getting_started)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Global seed set to 0\n"
     ]
    }
   ],
   "source": [
    "import scyan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "adata = scyan.read_fcs(\"<path-to-fcs>.fcs\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On this example, we have $N = 52 981$ cells and $M = 38$ markers (see below).\n",
    "Make sure your marker names looks good, or consider reading [`scyan.read_fcs`](../../api/read_fcs) for more advanced usage.\n",
    "\n",
    "If you have multiple `FCS`, consider [concatenating your data](https://anndata.readthedocs.io/en/latest/generated/anndata.AnnData.concatenate.html#anndata.AnnData.concatenate)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "AnnData object with n_obs × n_vars = 52981 × 38\n",
      "    obs: 'FSC-A', ..., 'Time'\n",
      "\n",
      "The markers names are: CD8, CD4, ...\n"
     ]
    }
   ],
   "source": [
    "print(adata)\n",
    "print(f\"\\nThe markers names are: {', '.join(adata.var_names)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Preprocess your data\n",
    "\n",
    "Choose either the `asinh` or `logicle` transformation below, and scale your data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# If you choose the logicle transformation (recommended)\n",
    "scyan.preprocess.auto_logicle_transform(adata)\n",
    "\n",
    "### If you choose the Asinh transformation\n",
    "#scyan.preprocess.asinh_transform(adata)\n",
    "\n",
    "scyan.preprocess.scale(adata) # To standardise your data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Creation of the knowledge table\n",
    "The knowledge table, or marker-population table, contains well-known marker expressions per population. For instance, if you want `Scyan` to annotate CD4 T cells, you have to tell which markers CD4 T cells are supposed to express or not. Depending on your panel, you may have CD4+, CD8-, CD45+, CD3+, etc. Values inside the table can be:\n",
    "\n",
    "- `-1` for negative expressions.\n",
    "- `1` for positive expressions.\n",
    "- Some float values such as `0` or `0.5` for mid and low expressions respectively (use it only when necessary).\n",
    "- `NA` when you don't know or if it is not applicable.\n",
    "\n",
    "We recommend the `csv` format for this table. You can either directly create a `csv`, or use Excel and export the table as `csv`.\n",
    "\n",
    "You can then import the `csv` to make a pandas `DataFrame`.\n",
    "\n",
    "### Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "marker_pop_matrix = pd.read_csv(\"<path-to-csv>.csv\", index_col=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>CD19</th>\n",
       "      <th>CD4</th>\n",
       "      <th>CD8</th>\n",
       "      <th>CD34</th>\n",
       "      <th>CD20</th>\n",
       "      <th>CD45</th>\n",
       "      <th>CD123</th>\n",
       "      <th>CD11c</th>\n",
       "      <th>CD7</th>\n",
       "      <th>CD16</th>\n",
       "      <th>CD38</th>\n",
       "      <th>CD3</th>\n",
       "      <th>HLA-DR</th>\n",
       "      <th>CD64</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>Populations</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>Basophils</th>\n",
       "      <td>-1</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>CD4 T cells</th>\n",
       "      <td>-1</td>\n",
       "      <td>1.0</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>CD8 T cells</th>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1</td>\n",
       "      <td>1.0</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>CD16- NK cells</th>\n",
       "      <td>-1</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1</td>\n",
       "      <td>1.0</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>CD16+ NK cells</th>\n",
       "      <td>-1</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>NaN</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>NaN</td>\n",
       "      <td>-1</td>\n",
       "      <td>-1.0</td>\n",
       "      <td>-1.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                CD19  CD4  CD8  CD34  CD20  CD45  CD123  CD11c  CD7  CD16  \\\n",
       "Populations                                                                 \n",
       "Basophils         -1  NaN -1.0    -1  -1.0   NaN      1     -1 -1.0  -1.0   \n",
       "CD4 T cells       -1  1.0 -1.0    -1  -1.0   NaN     -1     -1  NaN  -1.0   \n",
       "CD8 T cells       -1 -1.0  1.0    -1  -1.0   NaN     -1     -1  1.0  -1.0   \n",
       "CD16- NK cells    -1  NaN  NaN    -1  -1.0   NaN     -1     -1  1.0  -1.0   \n",
       "CD16+ NK cells    -1  NaN  NaN    -1   NaN   NaN     -1     -1  1.0   1.0   \n",
       "\n",
       "                CD38  CD3  HLA-DR  CD64  \n",
       "Populations                              \n",
       "Basophils        NaN   -1    -1.0  -1.0  \n",
       "CD4 T cells      NaN    1    -1.0  -1.0  \n",
       "CD8 T cells      NaN    1    -1.0  -1.0  \n",
       "CD16- NK cells   NaN   -1    -1.0  -1.0  \n",
       "CD16+ NK cells   NaN   -1    -1.0  -1.0  "
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "marker_pop_matrix.head() # Display the first 5 rows of the table"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see our [advice](../../advanced/advice) when creating this table.\n",
    "\n",
    "Also, ensure your column names correspond to marker names in `adata.var_names`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Creation of the `Scyan` model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO:scyan.model:Initialized Scyan model with N=52981 cells, P=29 populations and M=38 markers. No covariate provided.\n"
     ]
    }
   ],
   "source": [
    "model = scyan.Scyan(adata, marker_pop_matrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations! You can now follow our tutorial on [model training and visualisation](../usage)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. (Optional) Save your data for later use\n",
    "\n",
    "You can use [scyan.data.add](../../api/add) to save your data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "INFO:scyan.data.datasets:Creating new dataset folder at /.../your_project_name\n",
      "INFO:scyan.data.datasets:Created file /.../your_project_name/default.h5ad\n",
      "INFO:scyan.data.datasets:Created file /.../your_project_name/default.csv\n"
     ]
    }
   ],
   "source": [
    "scyan.data.add(\"your-project-name\", adata, marker_pop_matrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now simply load it with [scyan.data.load](../../api/load)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "adata, marker_pop_matrix = scyan.data.load(\"your-project-name\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.9.7 ('scyan-5lsXrWE1-py3.9')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "012c56802600048b57e0c4332009a32433bb12fbf6532686c4bcccd14306ea3b"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
