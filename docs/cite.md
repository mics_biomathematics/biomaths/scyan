This library has been developed by Quentin Blampey, PhD student in Biomathematics / Deep Learning. The following institutions funded this work:

- Lab of Mathematics and Computer Science (MICS), **CentraleSupélec** (Engineering School, Paris-Saclay University).
- PRISM center, **Gustave Roussy Institute** (Cancer campus, Paris-Saclay University).

It has been published in (**_not published yet_**).
