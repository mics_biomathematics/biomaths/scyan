# Hydra configuration (optional)

Using this folder is advanced and optional. See the [documentation](https://mics_biomathematics.pages.centralesupelec.fr/biomaths/scyan/advanced/hydra_wandb/) for more details.
