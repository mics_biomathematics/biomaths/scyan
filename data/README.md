# Data folder

This folder contains every data file used by the project and the tutorials.

See the [documentation](https://mics_biomathematics.pages.centralesupelec.fr/biomaths/scyan/advanced/data/) for more details or explanations for the creation of your own dataset.
